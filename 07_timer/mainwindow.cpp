#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    id1 = startTimer(1000);
    id2 = startTimer(500);

    QTimer *tmr1 = new QTimer(this);
    tmr1->start(1000);
    qDebug()<<"开启定时器";
    connect(tmr1, &QTimer::timeout,[=](){
        static int num = 0;

        qDebug()<<"定时时间到";
        ui->label_3->setText(QString::number(++num));
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerEvent(QTimerEvent *ev)
{
    static int num = 0;
    static int num2 = 0;
    if(ev->timerId() == id1)
        ui->label->setText(QString::number(num++));
    if(ev->timerId()==id2)
        ui->label_2->setText(QString::number(num2++));
}

