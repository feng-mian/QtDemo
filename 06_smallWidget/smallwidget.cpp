#include "smallwidget.h"
#include "ui_smallwidget.h"
#include <QSlider>
#include <QSpinBox>
#include <QDebug>
#include <QMouseEvent>

#if 1
#define dbg_printf qDebug
#else
#define dbg_printf(...)
#endif

smallWidget::smallWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::smallWidget)
{
    ui->setupUi(this);

    //联动滑动条
    //connect(ui->horizontalSlider,&QSlider::valueChanged,ui->spinBox,&QSpinBox::setValue);
    connect(ui->horizontalSlider,&QSlider::valueChanged,[=](int val){
        ui->spinBox->setValue(val);
        dbg_printf("触发QSlider changed信号");
    });

    //联动box
    void (QSpinBox:: * spinSignal)(int) = &QSpinBox::valueChanged;
    //connect(ui->spinBox, spinSignal,ui->horizontalSlider,&QSlider::setValue);
    connect(ui->spinBox, spinSignal,ui->horizontalSlider,[=](int val){
       ui->horizontalSlider->setValue(val);
       dbg_printf("触发QSpinBox changed信号");
    });
}

smallWidget::~smallWidget()
{
    delete ui;
}

int smallWidget::getSliderVal()
{
    return ui->spinBox->value();
}

int smallWidget::setSliderVal(int val)
{
    ui->spinBox->setValue(val);
    return 0;

}

void smallWidget::enterEvent(QEvent *)
{
    dbg_printf("鼠标进入控件");
}

void smallWidget::leaveEvent(QEvent *)
{
    dbg_printf("~鼠标离开控件");
}

void smallWidget::mouseMoveEvent(QMouseEvent *event)
{
    int x = event->x();
    int y = event->y();
    dbg_printf("鼠标按下移动,x= %d, y= %d",x,y);
}


