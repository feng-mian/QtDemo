### QMainWindow

1. 构成
   1. 标题
   2. 菜单栏 Menu Bar
   3. 工具栏 tool bar area
   4. 链接部件 dock widgets
   5. 中心部件 central widget
   6. 状态栏 status bar
2. 菜单栏 最多有一个
   1. QmenuBar * bar = MenuBar（）
   2. setMenuBar(bar)
   3. 创建菜单 QMenu *fileMenu = bar->addMenu("文件") 
   4. 创建菜单项 QAction *newAction =  fileMenu->addAction("新建")
   5. 添加分割线
3. 工具栏 可以有多个
   1. QToolBar *toolbar = new QTollBar（this）；
   2. addToolBar（默认停靠区域，toolbar）；
   3. 设置后期移动区域
   4. 禁止移动
   5. 添加控件