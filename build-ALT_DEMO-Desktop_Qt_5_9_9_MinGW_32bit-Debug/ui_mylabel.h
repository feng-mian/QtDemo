/********************************************************************************
** Form generated from reading UI file 'mylabel.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYLABEL_H
#define UI_MYLABEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyLabel
{
public:
    QLabel *labName;
    QLabel *labVal;

    void setupUi(QWidget *MyLabel)
    {
        if (MyLabel->objectName().isEmpty())
            MyLabel->setObjectName(QStringLiteral("MyLabel"));
        MyLabel->resize(337, 56);
        labName = new QLabel(MyLabel);
        labName->setObjectName(QStringLiteral("labName"));
        labName->setGeometry(QRect(10, 10, 100, 30));
        QFont font;
        font.setPointSize(14);
        labName->setFont(font);
        labVal = new QLabel(MyLabel);
        labVal->setObjectName(QStringLiteral("labVal"));
        labVal->setGeometry(QRect(140, 10, 130, 30));
        labVal->setFont(font);

        retranslateUi(MyLabel);

        QMetaObject::connectSlotsByName(MyLabel);
    } // setupUi

    void retranslateUi(QWidget *MyLabel)
    {
        MyLabel->setWindowTitle(QApplication::translate("MyLabel", "Form", Q_NULLPTR));
        labName->setText(QApplication::translate("MyLabel", "name", Q_NULLPTR));
        labVal->setText(QApplication::translate("MyLabel", "name", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MyLabel: public Ui_MyLabel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYLABEL_H
