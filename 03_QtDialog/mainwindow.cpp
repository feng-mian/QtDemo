#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDialog>
#include <QDebug>
#include <QAction>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //创建模态对话框
    connect(ui->actionNew, &QAction::triggered,[=](){
       //? 与new的区别
       QDialog dlg(this);
       dlg.resize(400,120);
       dlg.exec();
       qDebug() << "创建模态对话框";
    });

    //创建非模态对话框
    connect(ui->actionSave,&QAction::triggered,[=](){
        QDialog * dlg2 = new QDialog(this); //放入堆区,防止一闪而过
        dlg2->resize(400,120);
        dlg2->show();
        //防止一直打开不释放
        dlg2->setAttribute(Qt::WA_DeleteOnClose);
        qDebug() << "创建非模态对话框";
    });

    //标准对话框 消息对话框
    //MessageBox Dialog
    connect(ui->actionCritical,&QAction::triggered,[=](){
        QMessageBox::critical(this,"错误","消息内容");
    });

    connect(ui->actionInfo,&QAction::triggered,[=](){
        QMessageBox::information(this,"信息","消息内容");
    });

    connect(ui->actionwarning,&QAction::triggered,[=](){
        QMessageBox::warning(this,"警告","消息内容");

    });

    connect(ui->actionQuestion,&QAction::triggered,[=](){
        if(QMessageBox::Open ==QMessageBox::question(this,"选择","消息内容",QMessageBox::Open|QMessageBox::Save))
        {
            qDebug() << "选择的是打开";
            ui->actionNew->triggered();
        }
        else
        {
            qDebug() << "选择的是保存";
            ui->actionSave->triggered();
        }
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

