#include "mainwindow.h"
#include <QMenuBar>
#include <QToolBar>
#include <QPushButton>
#include <QStatusBar>
#include <QLabel>
#include <QDockWidget>
#include <QTextEdit>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    //resize
    resize(800,600);

    //menu bar
    //菜单栏最多有一个
    QMenuBar * bar = menuBar();
    //菜单栏放到窗口中
    setMenuBar(bar);

    //创建菜单
    QMenu * fileMenu = bar->addMenu("文件");
    QMenu * editMenu = bar->addMenu("编辑");

    //创建菜单项
    QAction * newAction = fileMenu->addAction("新建");
    QAction * saveAction = fileMenu->addAction("保存");
    //添加分隔符
    fileMenu->addSeparator();
    fileMenu->addAction("打开");


    //工具栏 可以有多个
    QToolBar *toolBar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea,toolBar);

    //设置 只允许 左右停靠
    toolBar->setAllowedAreas(Qt::LeftToolBarArea | Qt::RightToolBarArea);
    toolBar->setFloatable(false);
    toolBar->addAction(newAction);
    toolBar->addSeparator();
    toolBar->addAction(saveAction);

    QPushButton * btn = new QPushButton("关闭",this);
    connect(btn, &QPushButton::clicked, this, &QMainWindow::close);
    //添加控件
    toolBar->addWidget(btn);

    //状态栏
    QStatusBar *stBar = statusBar();
    //设置到窗口中
    setStatusBar(stBar);
    //放置标签
    //QLabel * label = new QLabel("提示信息",stBar);
     QLabel * label = new QLabel("提示信息",this);
     stBar->addWidget(label);

     //铆接部件 （浮动窗口）
     QDockWidget *dockWidget = new QDockWidget("链接窗口",this);
     addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
     dockWidget->setAllowedAreas(Qt::BottomDockWidgetArea|Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);

     //核心部件
     QTextEdit * textEdit = new QTextEdit("测试文本框",this);
     setCentralWidget(textEdit);
}

MainWindow::~MainWindow()
{
}

