/********************************************************************************
** Form generated from reading UI file 'chooselevelscene.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOOSELEVELSCENE_H
#define UI_CHOOSELEVELSCENE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChooseLevelScene
{
public:
    QAction *actionQuit;
    QWidget *centralwidget;
    QMenuBar *menubar;
    QMenu *menu;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ChooseLevelScene)
    {
        if (ChooseLevelScene->objectName().isEmpty())
            ChooseLevelScene->setObjectName(QStringLiteral("ChooseLevelScene"));
        ChooseLevelScene->resize(800, 600);
        actionQuit = new QAction(ChooseLevelScene);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        centralwidget = new QWidget(ChooseLevelScene);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        ChooseLevelScene->setCentralWidget(centralwidget);
        menubar = new QMenuBar(ChooseLevelScene);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 22));
        menu = new QMenu(menubar);
        menu->setObjectName(QStringLiteral("menu"));
        ChooseLevelScene->setMenuBar(menubar);
        statusbar = new QStatusBar(ChooseLevelScene);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        ChooseLevelScene->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menu->addAction(actionQuit);

        retranslateUi(ChooseLevelScene);

        QMetaObject::connectSlotsByName(ChooseLevelScene);
    } // setupUi

    void retranslateUi(QMainWindow *ChooseLevelScene)
    {
        ChooseLevelScene->setWindowTitle(QApplication::translate("ChooseLevelScene", "MainWindow", Q_NULLPTR));
        actionQuit->setText(QApplication::translate("ChooseLevelScene", "\351\200\200\345\207\272", Q_NULLPTR));
        menu->setTitle(QApplication::translate("ChooseLevelScene", "\345\274\200\345\247\213", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ChooseLevelScene: public Ui_ChooseLevelScene {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOOSELEVELSCENE_H
