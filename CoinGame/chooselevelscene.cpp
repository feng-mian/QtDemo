#include "chooselevelscene.h"
#include "ui_chooselevelscene.h"
#include <QPainter>
#include "mypushbutton.h"
#include <QTimer>
#include <QLabel>
#include <QSound>

ChooseLevelScene::ChooseLevelScene(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChooseLevelScene)
{


    ui->setupUi(this);
    //配置主场景
    //设置固定大小
    setFixedSize(320,588);

    //设置图标
    setWindowIcon(QIcon(":/res/Coin0001.png"));
    setWindowTitle("一个正经的测试工程");

    //painter 只能放到painter event中
    //创建返回按钮
    MyPushButton * btnClose = new MyPushButton(":/res/BackButton.png",":/res/BackButtonSelected.png",0);
    btnClose->setParent(this);
    btnClose->move(this->width()-btnClose->width(),this->height()-btnClose->height());
    //返回按钮的实现
    connect(btnClose, &MyPushButton::clicked, [=](){
        QTimer::singleShot(200,this,[=](){
            this->hide();
            //触发自定义信号
            emit chooseSceneBack();
        });
    });

    //退出按钮的实现
    connect(ui->actionQuit,&QAction::triggered,[=](){
        this->close();
    });

    /*--------------------创建关卡选择按钮--------------------*/
    for(int i=0; i<20 ;i++)
    {
        //新建按钮
        MyPushButton *btnMenu = new MyPushButton(":/res/LevelIcon.png");
        btnMenu->setParent(this);
        btnMenu->move(25+(i%4)*70, 130 + (i/4)*70);

        //按钮上显示文字
        QLabel *label = new QLabel;
        label->setParent(this);
        label->setText(QString::number(i+1));
        label->setFixedSize(btnMenu->width(),btnMenu->height());
        //居中
        label->setAlignment(Qt::AlignCenter);
        //位置
        label->move(25+(i%4)*70, 130 + (i/4)*70);
        label->setAttribute(Qt::WA_TransparentForMouseEvents,true);  //鼠标事件穿透

        //监听关卡按钮的信号曹
        //这里用错误的写法看看哪里错了
        connect(btnMenu,&MyPushButton::clicked,[=](){
            QTimer::singleShot(200,this,[=](){
                this->hide();
                pSence = new PlayScene(i+1);
                pSence->setGeometry(this->geometry());
                pSence->show();

                //点击之后关联返回函数
                connect(pSence,&PlayScene::chooseSceneBack,[=](){
                    this->setGeometry(pSence->geometry());
                    this->show();
                    delete pSence;
                    pSence = NULL;
                });
            });
        });
    }

}


ChooseLevelScene::~ChooseLevelScene()
{
    delete ui;
}

void ChooseLevelScene::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/res/OtherSceneBg.png");
    painter.drawPixmap(0,0,this->width(),this->height(),pix);


    //设置标题
    pix.load(":/res/Title.png");
    painter.drawPixmap((this->width()-pix.width())*0.5, 30, pix.width(), pix.height(),pix);
}
