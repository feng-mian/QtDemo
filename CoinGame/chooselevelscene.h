#ifndef CHOOSELEVELSCENE_H
#define CHOOSELEVELSCENE_H

#include <QMainWindow>
#include "playscene.h"
namespace Ui {
class ChooseLevelScene;
}

class ChooseLevelScene : public QMainWindow
{
    Q_OBJECT

public:
    explicit ChooseLevelScene(QWidget *parent = nullptr);
    ~ChooseLevelScene();
    void paintEvent(QPaintEvent *);
    PlayScene *pSence = NULL;

signals:
    void chooseSceneBack();

private:
    Ui::ChooseLevelScene *ui;
};

#endif // CHOOSELEVELSCENE_H
