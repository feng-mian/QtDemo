#include "mycoin.h"
#include <QDebug>
#include <QSound>

MyCoin::MyCoin(QString btnImg)
{
    QPixmap pix;
    bool ret = pix.load(btnImg);
    if(!ret)
    {
        qDebug()<<btnImg<<"图片加载失败";
    }
    this->setFixedSize(pix.width(),pix.height());
    this->setIcon(pix);
    this->setIconSize(QSize(pix.width(),pix.height()));
    //更改无边界
    this->setStyleSheet("QPushButton{border:0px;}");

    //创建定时器 用于翻转动画
    tmr021 = new QTimer(this);
    tmr120 = new QTimer(this);

    flipSound = new QSound(":/res/ConFlipSound.wav",this);

    //监听定时信号
    connect(tmr120,&QTimer::timeout,[=](){
        //更新图标
        QPixmap pix;
        QString str = QString(":/res/Coin000%1.png").arg(this->iconMin++);
        pix.load(str);
        this->setIcon(pix);
        //全部更新完毕停止定时器
        if(this->iconMin > this->iconMax)
        {
            iconMin = 1;
            tmr120->stop();
            isAnimation = false;
        }
    });

    connect(tmr021,&QTimer::timeout,[=](){
        //更新图标
        QPixmap pix;
        QString str = QString(":/res/Coin000%1.png").arg(this->iconMax--);
        pix.load(str);
        this->setIcon(pix);
        //全部更新完毕停止定时器
        if(this->iconMin > this->iconMax)
        {
            iconMax = 8;
            tmr021->stop();
            isAnimation = false;
        }
    });

//    //设置点击事件
//    connect(this,&MyCoin::clicked, [=](){
//        qDebug("icon被点击了,X: %d, Y: %d, flag: %d",posX,posY,flag);
//        changeFlag();
//    });
}

void MyCoin::changeFlag()
{

    if(flag == 0)
    {
        //021
        isAnimation = true;
        tmr021->start(30);
        flag = 1;
    }
    else
    {
        //120
        isAnimation = true;
        tmr120->start(30);
        flag = 0;
    }
}

void MyCoin::mousePressEvent(QMouseEvent *ev)
{
    //当动画执行时,禁止点击
    if(isAnimation)
    {
        return;
    }
    else
    {

        flipSound->play();
        return QPushButton::mousePressEvent(ev);
    }
}
