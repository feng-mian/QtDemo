#ifndef PLAYSCENE_H
#define PLAYSCENE_H

#include <QMainWindow>
#include "mycoin.h"
#include "dataconfig.h"

class PlayScene : public QMainWindow
{
    Q_OBJECT
public:
    explicit PlayScene(int levelNum);

    int levelIndex;
    //记录关卡数据
    int gameArray[4][4];
    void paintEvent(QPaintEvent *);

    //记录全部金币
    MyCoin *btnCoin[4][4];

    //胜利标志
    bool isWin = false;

    //金币翻面
    void changeCoin(int x, int y);

signals:
    void chooseSceneBack();

};

#endif // PLAYSCENE_H
