#include "mypushbutton.h"
#include <QDebug>
#include <QPropertyAnimation>
#include <QSound>

MyPushButton::MyPushButton(QString normalImg, QString pressImg,bool enAnimation)
{
    normalImgPath = normalImg;
    pressImgPath = pressImg;
    enZoom = enAnimation;

    //音效
    QSound *startSound = new QSound(":/res/TapButtonSound.wav",this);
    //加载图片
    QPixmap pix;
    bool ret = pix.load(normalImgPath);
    if(!ret)
    {
        qDebug() << "图片加载失败";
        return;
    }

    //设置固定尺寸
    setFixedSize(pix.width(),pix.height());

    //设置不规则图片样式
    setStyleSheet("QPushButton{border:0px}");
    //设置图标
    setIcon(pix);

    //设置图标大小
    setIconSize(QSize(pix.width(),pix.height()));

    //自定义点击事件
    //? connect(this,pressed,...) 和 mousePressEvent 的区别
//    connect(this,&QPushButton::pressed,[=](){
//        if(pressImgPath!="")
//        {

//            QPixmap pix;
//            bool ret = pix.load(pressImgPath);
//            if(!ret)
//            {
//                qDebug() << pressImgPath << "图片加载失败";
//            }
//            this->setFixedSize(pix.width(), pix.height());
//            this->setStyleSheet("QPushButton{border:0px}");
//            this->setIcon(pix);
//            this->setIconSize(QSize(pix.width(),pix.height()));

//        }
//    });
//    connect(this, &QPushButton::released,[=]()
//    {
//        if(normalImgPath!="")
//        {

//            QPixmap pix;
//            bool ret = pix.load(normalImgPath);
//            if(!ret)
//            {
//                qDebug() << pressImgPath << "图片加载失败";
//            }
//            this->setFixedSize(pix.width(), pix.height());
//            this->setStyleSheet("QPushButton{border:0px}");
//            this->setIcon(pix);
//            this->setIconSize(QSize(pix.width(),pix.height()));

//        }
//    });

    if(enZoom)
    connect(this,&QPushButton::clicked,[=](){
//        qDebug()<< "自定义按钮被点击";
        zoom(1);
        startSound->play();
        zoom(0);
    });
}

void MyPushButton::zoom(int  state)
{
    //创建动画特效
    QPropertyAnimation * animation = new QPropertyAnimation(this,"geometry");
    animation->setDuration(200);

    if(state)
    {
    //创建起始位置和结束位置 down
    animation->setStartValue(QRect(this->x(),this->y(),this->width(),this->height()));
    animation->setEndValue(QRect(this->x(),this->y()+10,this->width(),this->height()));
    }else
    {
        //创建起始位置和结束位置
        animation->setStartValue(QRect(this->x(),this->y()+10,this->width(),this->height()));
        animation->setEndValue(QRect(this->x(),this->y(),this->width(),this->height()));

    }
    //设置缓和曲线
    animation->setEasingCurve(QEasingCurve::OutBounce);
    animation->start();
}

void MyPushButton::mousePressEvent(QMouseEvent *e)
{
    if(pressImgPath!="")
    {

        QPixmap pix;
        bool ret = pix.load(pressImgPath);
        if(!ret)
        {
            qDebug() << pressImgPath << "图片加载失败";
        }
        this->setFixedSize(pix.width(), pix.height());
        this->setStyleSheet("QPushButton{border:0px}");
        this->setIcon(pix);
        this->setIconSize(QSize(pix.width(),pix.height()));
    }
    //交给父类执行按下事件
    return QPushButton::mousePressEvent(e);

}

void MyPushButton::mouseReleaseEvent(QMouseEvent * e)
{
    if(normalImgPath!="")
    {

        QPixmap pix;
        bool ret = pix.load(normalImgPath);
        if(!ret)
        {
            qDebug() << pressImgPath << "图片加载失败";
        }
        this->setFixedSize(pix.width(), pix.height());
        this->setStyleSheet("QPushButton{border:0px}");
        this->setIcon(pix);
        this->setIconSize(QSize(pix.width(),pix.height()));
    }
    //交给父类执行按下事件
    return QPushButton::mouseReleaseEvent(e);
}

