#include "playscene.h"
#include <QMenuBar>
#include <QPainter>
#include "mypushbutton.h"
#include <QLabel>
#include <QFont>
#include <QDebug>
#include <QPropertyAnimation>
#include <QSound>

PlayScene::PlayScene(int levelNum)
{
    levelIndex = levelNum;
    qDebug()<<"当前关卡为: "<< levelIndex;
    QMenuBar *bar = menuBar();
    this->setMenuBar(bar);
    QMenu * startMenu = bar->addMenu("开始");
    QAction * quitAction = startMenu->addAction("退出");
    connect(quitAction, &QAction::triggered,[=](){
       this->close();
    });
    //配置主场景
    //设置固定大小
    setFixedSize(320,588);

    //设置图标
    setWindowIcon(QIcon(":/res/Coin0001.png"));
    setWindowTitle("一个正经的测试工程");

    //返回按钮
    MyPushButton * btnClose = new MyPushButton(":/res/BackButton.png",":/res/BackButtonSelected.png",0);
    btnClose->setParent(this);
    btnClose->move(this->width()-btnClose->width(),this->height()-btnClose->height());
    connect(btnClose, &MyPushButton::clicked, [=](){
        this->hide();
        emit chooseSceneBack();
    });

    //设置胜利图片
    QLabel *winLabel = new QLabel(this);
    QPixmap winPix;
    winPix.load(":/res/LevelCompletedDialogBg.png");
    winLabel->setGeometry(0,0,winPix.width(),winPix.height());
    winLabel->setPixmap(winPix);
    winLabel->move((this->width()-winPix.width())*0.5,-winPix.height());

    //当前关卡标题
    QLabel *label = new QLabel(this);
    QFont font;
    font.setFamily("华文新魏");
    font.setPointSize(20);

    label->setFont(font);
    QString str = QString("LEVEL : %1").arg(this->levelIndex);
    label->setText(str);
    label->setGeometry(30,this->height()-50,120,50);

    //初始化关卡数据
    dataConfig config;
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            gameArray[i][j] = config.mData[this->levelIndex][i][j];
        }
    }

    //音效
    QSound *winSound = new QSound(":/res/LevelWinSound.wav",this);
    //创建金币背景图
    for (int i =0;i<4; i++)
    {
        for(int j = 0; j<4 ;j++ )
        {
            QLabel *label = new QLabel(this);
            label->setGeometry(0,0,50,50);
            label->setPixmap(QPixmap(":/res/BoardNode.png"));
            label->move(57+j*50,200+i*50);

            //创建金币
            MyCoin *coin;
            if(gameArray[i][j])
            {
                //data = 1
                coin = new MyCoin(":/res/Coin0001.png");
                coin->flag = 1;
            }else
            {
                //data = 0
                coin = new MyCoin(":/res/Coin0008.png");
                coin->flag = 0;
            }
            btnCoin[i][j] = coin;
            coin->posX = j;
            coin->posY = i;

            coin->setParent(this);
            coin->move(59+j*50,204+i*50);

            //连接点击事件
            connect(coin, &MyCoin::clicked,[=](){
                if(isWin == true)
                    return;
                //被点击的按钮翻转
                changeCoin(j,i);
                //周边按钮翻转
                changeCoin(j+1,i);
                changeCoin(j-1,i);
                changeCoin(j,i-1);
                changeCoin(j,i+1);

                //判断是否胜利
                QTimer::singleShot(300,this,[=](){
                    isWin = true;
                    for(int p=0; p <4 ;p++)
                    {
                        for(int q = 0; q<4 ;q++)
                        {
                            if(gameArray[p][q] == false)
                            {
                                isWin = false;
                                qDebug("false pos is X:%d, Y:%d",q,p);
                            }
                        }
                    }
                    if(isWin == true)
                    {
                        qDebug()<<"Win!";
                        winSound->play();
                        QPropertyAnimation *animation = new QPropertyAnimation(winLabel,"geometry");
                        animation->setDuration(1000);
                        animation->setStartValue(QRect(winLabel->x(),winLabel->y(),winLabel->width(),winLabel->height()));
                        animation->setEndValue(QRect(winLabel->x(),30,winLabel->width(),winLabel->height()));

                        animation->setEasingCurve(QEasingCurve::OutBounce);
                        animation->start();
                    }
                });


            });
        }
    }

}

void PlayScene::paintEvent(QPaintEvent *)
{
    //绘制背景和标题
    QPainter painter(this);
    QPixmap pix;

    pix.load(":/res/PlayLevelSceneBg.png");
    painter.drawPixmap(0,0,this->width(),this->height(),pix);

    pix.load(":/res/Title.png");
    painter.drawPixmap(10,30,pix.width(),pix.height(),pix);
}

void PlayScene::changeCoin(int x, int y)
{
    //gameArray[coin->posY][coin->posX]= coin->flag;
    if(x<0||x>=4)
        return;
    if(y<0||y>=4)
        return;

    btnCoin[y][x]->changeFlag();
    gameArray[y][x] = gameArray[y][x]?0:1;
}
