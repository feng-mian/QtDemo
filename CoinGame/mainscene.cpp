#include "mainscene.h"
#include "ui_mainscene.h"
#include <QPainter>
#include <QPushButton>
#include "mypushbutton.h"
#include <QTimer>
#include "chooselevelscene.h"
#include <QSound>

MainScene::MainScene(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainScene)
{
    ui->setupUi(this);
    //配置主场景
    //设置固定大小
    setFixedSize(320,588);

    //设置图标
    setWindowIcon(QIcon(":/res/Coin0001.png"));
    setWindowTitle("一个正经的测试工程");

    //退出按钮的实现
    connect(ui->actionQuit,&QAction::triggered,[=](){
        this->close();
    });

    //关卡选择界面
    ChooseLevelScene *SceneLevel = new ChooseLevelScene();
    //开始按钮
    MyPushButton *btnStart = new MyPushButton(":/res/MenuSceneStartButton.png");
    btnStart->setParent(this);
    btnStart->move((this->width()-btnStart->width())*0.5,this->height()*0.7);



    //开始按钮按键处理
    connect(btnStart, &MyPushButton::clicked,[=](){
        QTimer::singleShot(200,[=](){
           this->hide();
           SceneLevel->setGeometry(this->geometry());
           SceneLevel->show();
        });
    });

    //连接界面切换信号
    connect(SceneLevel, &ChooseLevelScene::chooseSceneBack, [=](){
        this->setGeometry(SceneLevel->geometry());
        this->show();
    });


}

MainScene::~MainScene()
{
    delete ui;
}

void MainScene::paintEvent(QPaintEvent *)
{
    //设置背景
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/res/PlayLevelSceneBg.png");
    painter.drawPixmap(0,0,this->width(),this->height(),pix);

    pix.load(":/res/Title.png");
    pix = pix.scaled(pix.width()* 0.5,pix.height()*0.5);
    painter.drawPixmap(10,30,pix);

}

