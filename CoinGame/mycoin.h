#ifndef MYCOIN_H
#define MYCOIN_H

#include <QWidget>
#include <QPushButton>
#include <QTimer>
#include <QSound>

class MyCoin : public QPushButton
{
    Q_OBJECT
public:
    explicit MyCoin(QString btnImg);
    //记录金币信息
    int posX;
    int posY;
    bool flag;  //正反标志

    //coin翻转扩展属性
    int iconMin = 1;
    int iconMax = 8;
    QTimer *tmr120;
    QTimer *tmr021;

    //coin是否在动画中
    bool isAnimation = false;

    QSound *flipSound;
    //改变coin正反
    void changeFlag();

    //拦截鼠标点击事件
    void mousePressEvent(QMouseEvent *);

signals:

};

#endif // MYCOIN_H
