#ifndef MYPUSHBUTTON_H
#define MYPUSHBUTTON_H

#include <QWidget>
#include <QPushButton>

class MyPushButton : public QPushButton
{
    Q_OBJECT
public:
    //explicit MyPushButton(QWidget *parent = nullptr);
    //默认值要么在h中给，要么在c中给，不能两个都有
    MyPushButton(QString normalImg, QString pressImg = "",bool enAnimation = 1);

    QString normalImgPath;
    QString pressImgPath;
    bool enZoom;

    //弹跳特效
    void zoom(int);
    //按下事件
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

signals:

};

#endif // MYPUSHBUTTON_H
