#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QListWidget>
#include <QStringList>
#include <QDebug>
#include <QTreeWidget>
#include <QTableWidget>

#define STACK_WIDGET_SIZE 3
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //stacked weidge btn
    connect(ui->btn_up,&QPushButton::clicked,[=]()
    {
        int index = ui->stackedWidget->currentIndex();

        ui->stackedWidget->setCurrentIndex(index>=STACK_WIDGET_SIZE?index=0:++index);
        qDebug("up按钮被按下，index是：%d",index);
    });

    connect(ui->btn_down,&QPushButton::clicked,[=]()
    {
        int index = ui->stackedWidget->currentIndex();

        ui->stackedWidget->setCurrentIndex(index<=0?index=STACK_WIDGET_SIZE:--index);
        qDebug("down按钮被按下，index是：%d",index);
    });

    //list widget
    QListWidgetItem * lwItem = new QListWidgetItem("刚翻过了几座山");
    ui->listWidget->addItem(lwItem);
    QStringList stringList;
    stringList << "是他"<<"是他"<<"就是他"<<"我们的朋友"<<"小哪吒";
    ui->listWidget->addItems(stringList);

    //tree widget
    ui->treeWidget->setHeaderLabels(QStringList()<< "编号"<< "更新模块"<<"需求"<<"响应时间");
    QTreeWidgetItem * treeItem = new QTreeWidgetItem(QStringList()<<"n/a"<<"曲线模块"<<"曲线需求"<<"2020/07/20");
    ui->treeWidget->addTopLevelItem(treeItem);
    QTreeWidgetItem * treeItem2 = new QTreeWidgetItem(QStringList()<<"1"<<""<<"可选择是压力曲线或者泄漏量曲线");
    treeItem->addChild(treeItem2);

    //table widget
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"序号"<<"姓名"<<"性别");
    ui->tableWidget->setRowCount(20);
    ui->tableWidget->setItem(0,1,new QTableWidgetItem("亚瑟王"));

}

MainWindow::~MainWindow()
{
    delete ui;
}

