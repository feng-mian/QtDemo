#include "mainwindow.h"

#include <QApplication>
#include <QDebug>

//extern "C"  qDebug(const char *message, ...)；
extern "C" void print(const char *message)
{
   qDebug(message);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

