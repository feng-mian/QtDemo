#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPushButton>
#include <QTimer>
#include "alos_logic/includes.h"
#include <mylabel.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qDebug("程序启动");

    MyLabel *mlbTest = new MyLabel("测试","值12f",this);
    mlbTest->move(20,20);

    //设备初始化
    alosBasic_init();
    connect(ui->btn_start,&QPushButton::clicked,[=](){
        qDebug()<<"开始命令";
        ucStartSignal = 1;

    });

    //设置定时器
    QTimer *tmrDemo = new QTimer(this);

    connect(tmrDemo,&QTimer::timeout,[=](){
        alosBasic_demo();
    });

    QTimer *tmrTick = new QTimer(this);

    connect(tmrDemo,&QTimer::timeout,[=](){
        ucTickSignal = 1;
    });

    tmrDemo->start(50);
    tmrTick->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

