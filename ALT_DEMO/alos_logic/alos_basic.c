/*
*********************************************************************************************************
*
*	模块名称 : alos基础模块
*	文件名称 : alos_basic.c
*	版    本 : V1.0
*	说    明 : 这是一个防水测试的基础模块,编写最基本的类和逻辑 
*	修改记录 :
*		V1.0    2020/06/30 Andy 开始编写
*
*	Copyright (C), 2020-2040, 方博科技(深圳)有限公司
*
*********************************************************************************************************
*/
//! 同级函数不进行相互调用
/******************************头文件注释******************************/

/******************************防重复引用******************************/

/******************************include区******************************/
#include "includes.h"
/******************************define区******************************/
#if 0
#define dbg_printf(...)
#define dgb_string(...)
#else
void print(const char *message);
#define dbg_string sprintf
#define dbg_printf print
char DBG_BUF[40];
#endif





/******************************全局变量******************************/

/******************************文件变量******************************/
BasicPhase_T tWaitPhase = {WAIT_PHASE_ID};
BasicPhase_T tFillPhase = {FILL_PHASE_ID};
BasicPhase_T tStabilizationPhase = {STAB_PHASE_ID};
BasicPhase_T tTestPhase = {TEST_PHASE_ID};
BasicPhase_T tDumpPhase = {DUMP_PHASE_ID};

//BasicPhase_T *ptCurrentPhase;
/* 防水主对象 */
alos_T tAlos;
/******************************函数声明******************************/
//* 外部函数声明 */
#pragma region
uint8_t ucaEvState[2] = {0};
/* 控制一个电磁阀 */
void ev_single_ctr(uint8_t _ucNum, uint8_t _ucVal)
{
    dbg_string(DBG_BUF,"ev num : %d, state is %d \r\n", _ucNum, _ucVal);
    dbg_printf(DBG_BUF);
}

/* 控制多个电磁阀 */
void ev_multi_ctr(uint8_t _ucArea, uint8_t _ucNum, uint8_t _ucVal)
{
    int i = 0, j = 0;
    for (j = 0; j <= _ucArea; j++)
    {
        for (i = 0; i < 8; i++)
        {
            if (_ucNum & (0x01 << i))
            {
                uint8_t val = _ucVal & (0x01 << i);
                val = val ? 1 : 0;
                dbg_string(DBG_BUF,"ev num: %d, state is %d, area is %d\r\n", i, val, j);
                dbg_printf(DBG_BUF);
            }
        }
    }
}

/* 获取气压值 */
float sensor_pressure_get()
{
    static float fVal = 0;
    fVal++;
 //   dbg_string("sensor pressure get : %f\r\n", fVal);
    return fVal;
}
#pragma endregion

//* 内部函数实现 */
#pragma region
void alos_set_result(alosResult_T _tResult)
{
    tAlos.tResult = _tResult;
}

void alos_change_phase(BasicPhase_T *_ptPhase)
{
    tAlos.ptCurrentPhase->tOpt.exit();
    tAlos.ptCurrentPhase = _ptPhase;
    tAlos.ptCurrentPhase->tOpt.enter();
}

/* 中断测试阶段 */
void alos_break_phase()
{
    tAlos.ptCurrentPhase->tOpt.release();
    tAlos.ptCurrentPhase = &tDumpPhase;
    tAlos.ptCurrentPhase->tOpt.enter();
}

/* 操作函数注册 */
void basicPhase_regeister_opt(BasicPhase_T *_ptPhase, BasicPhaseOpt_T _tOpt)
{
    _ptPhase->tOpt = _tOpt;
}

#pragma endregion
/****************************Phase Opt实现***************************/
void invalid_opt()
{
    dbg_string(DBG_BUF,"This is invalid operate!\r\n");
    dbg_printf(DBG_BUF);
}

//* wait phase operate
#pragma region
void waitPhase_enter(void)
{
    dbg_printf("IN waitphase\r\n");
    /* 参数初始化 */
    tWaitPhase.tData.ulUsedTime = 0;
    tWaitPhase.tData.fExitVal = 0;
    tWaitPhase.tData.fEnterVal = 0;
    /* 默认状态电磁阀 */
    ev_multi_ctr(0, 0b00000111, 0b00000101);
    /* 一帮情况下和上一个阶段的exit相同 */
    /* 故一般只执行exit */
    /* 记录进入气压 */
    tWaitPhase.tData.fEnterVal = sensor_pressure_get();
}

void waitPhase_tick(void)
{
    alosResult_T tResult = {0};

    tWaitPhase.tData.fCurrentVal = sensor_pressure_get();
    //todo 记录气压值到曲线/链表 */

    /* 判断是否不在范围 */
    if (tWaitPhase.tData.fCurrentVal < tWaitPhase.tPara.fMinFill)
    {
        /* pressure range err */
        tResult.ucLeakLevel = WAIT_RANGE_MIN;
        alos_set_result(tResult);
        return;
    }
    if (tWaitPhase.tData.fCurrentVal > tWaitPhase.tPara.fMaxFill)
    {
        /* pressure range err */
        tResult.ucLeakLevel = WAIT_RANGE_MAX;
        alos_set_result(tResult);
        return;
    }
}

void waitPhase_exit(void)
{
    dbg_printf("EXIT waitPhase\r\n");
    /* 记录数据 */
    tWaitPhase.tData.fExitVal = tWaitPhase.tData.fCurrentVal;
    /* 电磁阀动作 */
    /* 由下一个流程的enter规定 */
}
void waitPhase_Release(void)
{
    /* 无效项目 */
    invalid_opt();
}
BasicPhaseOpt_T tWaitPhaseOpt =
    {
        .enter = waitPhase_enter,
        .tick = waitPhase_tick,
        .exit = waitPhase_exit,
        .release = waitPhase_Release};
#pragma endregion

//* fill phase operate */
#pragma region
void fillPhase_enter(void)
{
    dbg_printf("ENTER fillPhase\r\n");
    /* 默认状态电磁阀 */
    /* 阶段初始化 */
    tFillPhase.tData.ulUsedTime = 0;
    tFillPhase.tData.fEnterVal = 0;
    tFillPhase.tData.fExitVal = 0;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000011);
    /* 气压记录 */
    tFillPhase.tData.fEnterVal = sensor_pressure_get();
}

void fillPhase_tick(void)
{
    alosResult_T tResult = {0};
    //! 阶段切换的时候,当前气压CurrentVal可能会出现跳变的显示异常
    tFillPhase.tData.fCurrentVal = sensor_pressure_get();
    //todo 记录气压值到曲线/链表 */

    /* 判断是否不在范围 */
    if (tFillPhase.tData.fCurrentVal < tFillPhase.tPara.fMinFill)
    {
        dbg_printf("PRE LOW Fill Phase\r\n");
        /* pressure range err */
        tResult.ucLeakLevel = LEAK_RANGE_MIN;
        alos_set_result(tResult);
        /* 切换到排气流程测试流程 */
        alos_break_phase();
        return;
    }
    if (tFillPhase.tData.fCurrentVal > tFillPhase.tPara.fMaxFill)
    {
        dbg_printf("PRE HIGH Fill Phase\r\n");
        /* pressure range err */
        tResult.ucLeakLevel = LEAK_RANGE_MAX;
        alos_set_result(tResult);
        /* 切换到排气流程测试流程 */
        alos_break_phase();
        return;
    }

    //* 气压符合范围
    /* 更新泄露值,仅测试阶段 */
    //tResult.fLeakVal = tFillPhase.tData.fCurrentVal - tFillPhase.tData.fEnterVal;
    /* 判断是否到时 */
    tFillPhase.tData.ulUsedTime += TICK_PERIOD;
    if (tFillPhase.tData.ulUsedTime >= tFillPhase.tPara.ulTotalTime)
    {
        dbg_printf("TIMEOUT Fill Phase\r\n");
        /* 切换下一个流程 */
        alos_change_phase(&tStabilizationPhase);
    }
}

void fillPhase_exit(void)
{
    dbg_printf("EXIT fill phase \r\n");
    /* 记录数据 */
    tFillPhase.tData.fExitVal = tFillPhase.tData.fCurrentVal;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000010);
}

void fillPhase_Release(void)
{
    alosResult_T tResult = {0};
    dbg_printf("FREE fill phase\r\n");
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000011);
    /* 定时器关闭 */
    //todo os定时器关闭
    //dbg_printf("inf:充气阶段定时器关闭\r\n");
    /* 结果标记 */
    //!更改到tick中做
    /* 跳转到排气阶段 */
    //!更改到上一级函数做
}

BasicPhaseOpt_T tFillPhaseOpt =
    {
        .enter = fillPhase_enter,
        .tick = fillPhase_tick,
        .exit = fillPhase_exit,
        .release = fillPhase_Release};

#pragma endregion

//* Stabilization phase operate */
#pragma region
void stabilizationPhase_enter(void)
{
    dbg_printf("ENTER stabilization phase\r\n");
    /* 阶段初始化 */
    tStabilizationPhase.tData.ulUsedTime = 0;
    tStabilizationPhase.tData.fEnterVal = 0;
    tStabilizationPhase.tData.fExitVal = 0;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000010);
    /* 气压记录 */
    tStabilizationPhase.tData.fEnterVal = sensor_pressure_get();
}

void stabilizationPhase_tick(void)
{
    alosResult_T tResult = {0};

    tStabilizationPhase.tData.fCurrentVal = sensor_pressure_get();
    //todo 记录气压值到曲线/链表 */

    /* 判断是否不在范围 */
    if (tStabilizationPhase.tData.fCurrentVal < tStabilizationPhase.tPara.fMinFill)
    {
        dbg_printf("PRE LOW stabilization phase\r\n");
        /* pressure range err */
        tResult.ucLeakLevel = LEAK_RANGE_MIN;
        alos_set_result(tResult);
        /* 切换到排气流程测试流程 */
        alos_break_phase();
        return;
    }
    if (tStabilizationPhase.tData.fCurrentVal > tStabilizationPhase.tPara.fMaxFill)
    {
        dbg_printf("PRE HIGH stabilization phase\r\n");
        /* pressure range err */
        tResult.ucLeakLevel = LEAK_RANGE_MAX;
        alos_set_result(tResult);
        /* 切换到排气流程测试流程 */
        alos_break_phase();
        return;
    }

    //* 气压符合范围
    /* 判断是否到时 */
    tStabilizationPhase.tData.ulUsedTime += TICK_PERIOD;
    if (tStabilizationPhase.tData.ulUsedTime >= tStabilizationPhase.tPara.ulTotalTime)
    {
        dbg_printf("TIMEOUT stabilization phase\r\n");
        /* 切换下一个流程 */
        alos_change_phase(&tTestPhase);
    }
}

void stabilizationPhase_exit(void)
{
    dbg_printf("EXIT stabilization phase\r\n");
    /* 记录数据 */
    tStabilizationPhase.tData.fExitVal = tStabilizationPhase.tData.fCurrentVal;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000010);
}
void stabilizationPhase_Release(void)
{
    alosResult_T tResult = {0};
    dbg_printf("FREE stabilization phase\r\n");
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000011);
    /* 定时器关闭 */
    //todo os定时器关闭
    //dbg_printf("inf:平衡阶段定时器关闭\r\n");
    /* 结果标记 */
    //!更改到tick中做
    /* 跳转到排气阶段 */
    //!更改到上一级函数做
}

BasicPhaseOpt_T tStabilizationPhaseOpt =
    {
        .enter = stabilizationPhase_enter,
        .tick = stabilizationPhase_tick,
        .exit = stabilizationPhase_exit,
        .release = stabilizationPhase_Release};

#pragma endregion

//* Test phase operate */
#pragma region
void test_enter(void)
{
    dbg_printf("ENTER test phase\r\n");
    /* 阶段初始化 */
    tTestPhase.tData.ulUsedTime = 0;
    tTestPhase.tData.fEnterVal = 0;
    tTestPhase.tData.fExitVal = 0;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000010);
    /* 气压记录 */
    tTestPhase.tData.fEnterVal = sensor_pressure_get();
}

void test_tick(void)
{
    alosResult_T tResult = {0};

    tTestPhase.tData.fCurrentVal = sensor_pressure_get();
    //todo 记录气压值到曲线/链表 */

    /* 判断是否不在范围 */
    if (tTestPhase.tData.fCurrentVal < tTestPhase.tPara.fMinFill)
    {
        dbg_printf("PRE LOW test phase\r\n");
        /* pressure range err */
        tResult.ucLeakLevel = LEAK_RANGE_MIN;
        alos_set_result(tResult);
        /* 切换到排气流程测试流程 */
        alos_break_phase();
        return;
    }
    if (tTestPhase.tData.fCurrentVal > tTestPhase.tPara.fMaxFill)
    {
        dbg_printf("PRE HIGH test phase\r\n");
        /* pressure range err */
        tResult.ucLeakLevel = LEAK_RANGE_MAX;
        alos_set_result(tResult);
        /* 切换到排气流程测试流程 */
        alos_break_phase();
        return;
    }

    //* 气压符合范围
    /* 更新实时泄漏量 */
    tResult.fLeakVal = tTestPhase.tData.fCurrentVal - tTestPhase.tData.fEnterVal;
    alos_set_result(tResult);
    /* 判断是否到时 */
    tTestPhase.tData.ulUsedTime += TICK_PERIOD;
    if (tTestPhase.tData.ulUsedTime >= tTestPhase.tPara.ulTotalTime)
    {
        dbg_printf("TIMEOUT test phase\r\n");
        /* 切换下一个流程 */
        alos_change_phase(&tDumpPhase);
    }
}

void test_exit(void)
{
    alosResult_T tResult = {0};
    dbg_printf("EXIT test phase\r\n");
    /* 记录数据 */
    tTestPhase.tData.fExitVal = tTestPhase.tData.fCurrentVal;
    /* 更新测试泄漏量 */
    tResult.fLeakVal = tTestPhase.tData.fExitVal - tTestPhase.tData.fEnterVal;
    if (tResult.fLeakVal < tTestPhase.tPara.fLeakMin)
    {
        tResult.ucLeakLevel = LEAK_FAIL;
    }
    else if (tResult.fLeakVal > tTestPhase.tPara.fLeakMax)
    {
        tResult.ucLeakLevel = LEAK_FAIL;
    }
    else
    {
        tResult.ucLeakLevel = LEAK_PASS;
    }
    alos_set_result(tResult);
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000011);
}
void test_Release(void)
{
    alosResult_T tResult = {0};
    dbg_printf("FREE test phase\r\n");
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000011);
    /* 定时器关闭 */
    //todo os定时器关闭
    //dbg_printf("inf:测试阶段定时器关闭\r\n");
    /* 结果标记 */
    //!更改到tick中做
    /* 跳转到排气阶段 */
    //!更改到上一级函数做
}

BasicPhaseOpt_T tTestPhaseOpt =
    {
        .enter = test_enter,
        .tick = test_tick,
        .exit = test_exit,
        .release = test_Release};

#pragma endregion

//* Dump phase operate */
#pragma region
void dump_enter(void)
{
    dbg_printf("ENTER dump phase\r\n");
    /* 阶段初始化 */
    tDumpPhase.tData.ulUsedTime = 0;
    tDumpPhase.tData.fEnterVal = 0;
    tDumpPhase.tData.fExitVal = 0;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000011);
    /* 气压记录 */
    tDumpPhase.tData.fEnterVal = sensor_pressure_get();
}

void dump_tick(void)
{
    alosResult_T tResult = {0};

    tDumpPhase.tData.fCurrentVal = sensor_pressure_get();
    //todo 记录气压值到曲线/链表 */

    /* 判断是否到时 */
    tDumpPhase.tData.ulUsedTime += TICK_PERIOD;
    if (tDumpPhase.tData.ulUsedTime >= tDumpPhase.tPara.ulTotalTime)
    {
        dbg_printf("TIME dump phase\r\n");
        /* 切换静态流程 */
        alos_change_phase(&tWaitPhase);
    }
}

void dump_exit(void)
{
    alosResult_T tResult = {0};
    dbg_printf("EXIT dump phase\r\n");
    /* 记录数据 */
    tDumpPhase.tData.fExitVal = tDumpPhase.tData.fCurrentVal;
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000101);
}
void dump_Release(void)
{
    alosResult_T tResult = {0};
    dbg_printf("FREE dump phase\r\n");
    /* 电磁阀动作 */
    ev_multi_ctr(0, 0b00000111, 0b00000101);
    /* 定时器关闭 */
    //todo os定时器关闭
    //dbg_printf("inf:排气阶段定时器关闭\r\n");

    //?更改状态为wait
}

BasicPhaseOpt_T tDumpPhaseOpt =
    {
        .enter = dump_enter,
        .tick = dump_tick,
        .exit = dump_exit,
        .release = dump_Release};

#pragma endregion

/******************************函数实现******************************/
void alosBasic_init()
{
    /* 对阶段结构体初始化,后期改成读文件进行初始化 */
    /* 可以分成 cfg, parameter, fun三个部分 */
    BasicPhasePara_T tTmpPara = {
        .fMaxFill = 35000,
        .fMinFill = 0,
        .fLeakMax = 100,
        .fLeakMin = 10,
        .ulTotalTime = 5000};

    tWaitPhase.tPara = tTmpPara;
    tFillPhase.tPara = tTmpPara;
    tStabilizationPhase.tPara = tTmpPara;
    tTestPhase.tPara = tTmpPara;
    /* 注册阶段操作函数 */
    basicPhase_regeister_opt(&tWaitPhase, tWaitPhaseOpt);
    basicPhase_regeister_opt(&tFillPhase, tFillPhaseOpt);
    basicPhase_regeister_opt(&tStabilizationPhase, tStabilizationPhaseOpt);
    basicPhase_regeister_opt(&tTestPhase, tTestPhaseOpt);
    basicPhase_regeister_opt(&tDumpPhase, tDumpPhaseOpt);
    /* 执行参数初始化 */
    tAlos.ptCurrentPhase = &tWaitPhase;
    tAlos.ptCurrentPhase->tOpt.enter();
}


void start_handle(void)
{
    // /* 取出alos中的phase, 方便表述 */
    // BasicPhase_T *_ptPhase;
    // _ptPhase = _pAlos->ptCurrentPhase;
    // if (_ptPhase != NULL)
    //     return;

    // _ptPhase = &tWaitPhase;
    // _ptPhase->tOpt.enter();

    /* 接收到启动命令 */
    if (tAlos.ptCurrentPhase != &tWaitPhase)
    {
        dbg_printf("receive cmd start, But system is running!\r\n");
        return;
    }

    tAlos.ptCurrentPhase = &tFillPhase;
    tAlos.ptCurrentPhase->tOpt.enter();
    //todo 启动定时器
    dbg_printf("receive cmd start ,enter FillPhase\r\n");
}

void stop_handle(void)
{
    /* 收到停止命令 */
    if (tAlos.ptCurrentPhase == &tWaitPhase)
    {
        dbg_printf("receive cmd stop, But system is waitting\r\n");
        return;
    }

    alos_break_phase();
    tAlos.tResult.ucLeakLevel = LEAK_USER_STOP;
    //todo 停止定时器
    dbg_printf("receive cmd stop,stop now\r\n");
}

void tick_handle(void)
{
    /* 定时器触发 */
    tAlos.ptCurrentPhase->tOpt.tick();
    /* 调试器显示 */
    //dbg_view();
}

//ALOS DEMO
uint8_t ucStartSignal = 0;
uint8_t ucStopSignal = 0;
uint8_t ucTickSignal = 0;
void alosBasic_demo()
{

    if (ucStartSignal)
    {
        ucStartSignal = 0;
        start_handle();
    }

    if (ucStopSignal)
    {
        ucStopSignal = 0;
        stop_handle();
    }

    if (ucTickSignal)
    {
        ucTickSignal = 0;
        tick_handle();
    }

    /* 如果没有在动作phase中,定期刷新屏幕AD值 */
    /* os api */
}
/******************************文件尾注释******************************/
