#ifndef _ALOS_BASIC_H
#define _ALOS_BASIC_H

#ifdef __cplusplus
    extern "C"{
#endif

/******************************define区******************************/
/* unit: ms */
#define TICK_PERIOD 100
/* Leak Level 声明 */
#define LEAK_PASS 1
#define LEAK_FAIL 2
#define LEAK_USER_STOP 3
#define LEAK_RANGE_MAX 4
#define LEAK_RANGE_MIN 5
#define WAIT_RANGE_MAX 6
#define WAIT_RANGE_MIN 7

/* Basic Phase Id */
typedef enum
{
    WAIT_PHASE_ID = 0,
    FILL_PHASE_ID,
    STAB_PHASE_ID,
    TEST_PHASE_ID,
    DUMP_PHASE_ID
} BasicPhaseId_E;

/******************************enum声明******************************/

/******************************类型声明******************************/
//* 基础阶段类
#pragma region
/* 防水测试阶段操作 opt*/
typedef struct tagBasicPhaseOpt_T
{
    void (*enter)(void);
    void (*tick)(void);
    void (*exit)(void);
    void (*release)(void);
} BasicPhaseOpt_T;

/* 防水测试基础阶段参数 para*/
typedef struct tagBasicPhasePara_T
{
    float fMaxFill; /* 气压上下限 */
    float fMinFill;
    uint32_t ulTotalTime; /* 总时间 */
    /* 结果判断 */
    //uint8_t ucJudege;
    float fLeakMax;
    float fLeakMin;
} BasicPhasePara_T;

/* 防水测试基础阶段数据 data */
typedef struct tagBasicPhaseData_T
{
    uint32_t ulUsedTime; /* 已用时间 */
    float fEnterVal;
    float fExitVal;
    float fCurrentVal;
} BasicPhaseData_T;

/*  防水测试基础阶段 phase */
typedef struct tagBasicPhase_T
{
    /* 阶段名称 */
    BasicPhaseId_E eId;
    /* 阶段参数 */
    BasicPhasePara_T tPara;
    /* 阶段数据 */
    BasicPhaseData_T tData;
    /* 操作函数 */
    BasicPhaseOpt_T tOpt;
} BasicPhase_T;
#pragma endregion

//* 防水测试类
#pragma region
/* 防水测试结果 */
typedef struct tagResult_T
{
    /* 泄露值 */
    float fLeakVal;
    /* 泄漏等级:pass fail */
    uint8_t ucLeakLevel;
} alosResult_T;

/*防水测试类 */
typedef struct tagALos_T
{
    /* 当前阶段 */
    BasicPhase_T *ptCurrentPhase;
    /* 测试结果 */
    alosResult_T tResult;
} alos_T;
#pragma endregion
/******************************函数声明******************************/

extern uint8_t ucStartSignal;
extern uint8_t ucStopSignal;
extern uint8_t ucTickSignal;

void alosBasic_init(void);
void alosBasic_demo(void);


//变量声明
extern BasicPhase_T tWaitPhase;
extern BasicPhase_T tFillPhase;
extern BasicPhase_T tStabilizationPhase;
extern BasicPhase_T tTestPhase;
extern BasicPhase_T tDumpPhase;

//BasicPhase_T *ptCurrentPhase;
/* 防水主对象 */
extern alos_T tAlos;
#ifdef __cplusplus
    }
#endif
#endif
