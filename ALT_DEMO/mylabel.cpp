#include "mylabel.h"
#include "ui_mylabel.h"



MyLabel::MyLabel(const QString &name, const QString &val, QWidget *parent) :
    QLabel(parent),
    ui(new Ui::MyLabel)
{
    ui->setupUi(this);

    setFixedSize(250,40);
    ui->labName->setGeometry(10,5,100,30);
    ui->labVal->setGeometry(QRect(110,5,130,30));

    //给label赋值
    ui->labName->setText(name);
    ui->labVal->setText(val);
}

MyLabel::~MyLabel()
{
    delete ui;
}
