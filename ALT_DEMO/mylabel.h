#ifndef MYLABEL_H
#define MYLABEL_H

#include <QWidget>
#include <QString>
#include <QLabel>

namespace Ui {
class MyLabel;
}

class MyLabel : public QLabel
{
    Q_OBJECT

public:
    explicit MyLabel(const QString &name, const QString &val,QWidget *parent = nullptr);
    ~MyLabel();
private:
    Ui::MyLabel *ui;
};

#endif // MYLABEL_H
